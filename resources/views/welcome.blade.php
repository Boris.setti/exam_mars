@extends('layouts.app')

@section('content')
<div class="welcome">

    <h1>Et ça te dit de ne pas mourir bêtement ?</h1>
    <h3>Grace à cette appli tu pourras voir où se trouvent les minerais de Klingon, Chomdû, Perl ou un minerai inconnu en un click.</h3>
    <a href='map' type="button" class="btn btn-primary">Carte</a>
    <h3>Mieux, aide-nous à les référencer !</h3>
    @guest
    <a href='login' type="button" class="btn btn-primary">inscription</a>
     
    @else
        <a href="showmin" type="button" class="btn btn-primary">Ajouter un minerai </a>
    @endguest

</div>



@endsection