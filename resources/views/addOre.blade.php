@extends('layouts.app')

@section('content')
<div class="container">

    <div class="col">

        <form action="addmin" method="post">
            @csrf
            <div class="form-group">
                <label for="type_min">Type de minerai détecter</label>
                <select class="form-control" id="type_min" name="type_min" required>
                    <option></option>
                    <option>Klingon</option>
                    <option>Chomdû</option>
                    <option>Perl</option>
                    <option>Inconnu</option>
                </select>

                <div class="form-group">
                    <label for="dangerousness">Dangerosité du minerai détecter</label>
                    <select class="form-control" id="dangerousness" name="dangerousness" required>
                        @for($i = 1;$i <= 10 ;$i++) <option><?= $i ?></option>

                            @endfor
                    </select>
                    <h4>cliquer sur la carte pour déterminer l'emplacement du minerai</h4>
                    <div class="map-form " id="map"></div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="latitude">Latitude</label>
                                <p id="text-lat"></p>
                                <input type="hidden" class="form-control" id="latitude" name="lat" step="0.000000000000000000001" required>
                            </div>
                        </div>
                        <div class="col"></div>
                        <div class="form-group">
                            <label for="longitude">longitude</label>
                            <p id="text-lng"></p>
                            <input type="hidden" class="form-control" id="longitude" name="lng" step="0.000000000000000000001" required>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="com">Commentaire</label>
                    <textarea class="form-control" id="com" rows="3" name="com" required></textarea>
                </div>

                <button class="btn btn-primary" type="submit">Valider</button>

        </form>

    </div>
</div>
<?php
$tabZone = [];
foreach ($zone as $zones) {
    $infoZone = [
        'id' => $zones['id'],
        'user_id' => $zones['user_id'],
        'type_min' => $zones['type_min'],
        'dangerousness' => $zones['dangerousness'],
        'geo_lat' => $zones['geo_lat'],
        'geo_lng' => $zones['geo_lng'],
        'commentary' => $zones['commentary'],
        'created_at' => $zones['created_at'],
        'name'=>$zones['name'],
    ];
    array_push($tabZone, $infoZone);
}
?>


<script type="application/javascript">
    var zoneData = <?php echo json_encode($tabZone); ?>;
    </script>
    <script  type="application/javascript" src="js/map_view_addOre.js"></script>

@endsection