@extends('layouts.app')
@section('content')
<div>
<h1>Voici la carte des minerais recensés</h1>
</div>


<div class='map' id="map"></div>


<?php
$tabZone = [];
foreach ($zone as $zones) {
    $infoZone = [
        'id' => $zones['id'],
        'user_id' => $zones['user_id'],
        'type_min' => $zones['type_min'],
        'dangerousness' => $zones['dangerousness'],
        'geo_lat' => $zones['geo_lat'],
        'geo_lng' => $zones['geo_lng'],
        'commentary' => $zones['commentary'],
        'created_at' => $zones['created_at'],
        'name'=>$zones['name'],
    ];
    array_push($tabZone, $infoZone);
}
?>

<script type="application/javascript">
    var zoneData = <?php echo json_encode($tabZone); ?>;
</script>
<script  type="application/javascript" src="js/map_view_map.js"></script>
@endsection