<?php

namespace App\Http\Controllers;


use App\User;
use App\dangerousZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Ore extends Controller
{
    public function showForm()
    {
        $dZone = dangerousZone::addSelect(['name' => User::select('name')->whereColumn('user_id', 'id')])->get();

        return view('addOre', [
            "zone" => $dZone
        ]);
    }
    public function addOre()
    {


        $dZone = new dangerousZone();

        $dZone->user_id = Auth::user()->id;
        $dZone->type_min = Request('type_min');
        $dZone->dangerousness = Request('dangerousness');
        $dZone->geo_lat = Request('lat');
        $dZone->geo_lng = Request('lng');
        $dZone->commentary = Request('com');

        $dZone->save();
        return redirect('map');
    }
}
