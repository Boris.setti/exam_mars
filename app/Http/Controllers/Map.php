<?php

namespace App\Http\Controllers;

use App\User;
use App\dangerousZone;
use Illuminate\Http\Request;

class Map extends Controller
{
    public function map()
    {
        // $dZone = dangerousZone::all();
        $dZone =dangerousZone::addSelect(['name' => User::select('name')->whereColumn('user_id','id')])->get();

        return view('map', [
            "zone" => $dZone
        ]);
    }
}
