var klingon = L.icon({
    iconUrl: 'images/klingon.png',

    iconSize: [25, 25], // size of the icon
    shadowSize: [25, 25], // size of the shadow
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    shadowAnchor: [0, 0], // the same for the shadow
    popupAnchor: [12, 0] // point from which the popup should open relative to the iconAnchor
});

var anpe = L.icon({
    iconUrl: 'images/Pole_Emploi.png',

    iconSize: [35, 25], // size of the icon
    shadowSize: [25, 25], // size of the shadow
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    shadowAnchor: [0, 0], // the same for the shadow
    popupAnchor: [15, 0] // point from which the popup should open relative to the iconAnchor
});

var perl = L.icon({
    iconUrl: 'images/perl.png',

    iconSize: [25, 25], // size of the icon
    shadowSize: [25, 25], // size of the shadow
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    shadowAnchor: [0, 0], // the same for the shadow
    popupAnchor: [14, 0] // point from which the popup should open relative to the iconAnchor
});

var warning = L.icon({
    iconUrl: 'images/warning.png',

    iconSize: [25, 25], // size of the icon
    shadowSize: [25, 25], // size of the shadow
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    shadowAnchor: [0, 0], // the same for the shadow
    popupAnchor: [14, 0] // point from which the popup should open relative to the iconAnchor
});

var city = L.icon({
    iconUrl: 'images/city.png',

    iconSize: [70, 70], // size of the icon
    shadowSize: [70, 70], // size of the shadow
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    shadowAnchor: [0, 0], // the same for the shadow
    popupAnchor: [35, 0] // point from which the popup should open relative to the iconAnchor
});

var baseMars = [0, 0]
    // creation de la map
var map = L.map('map').setView(baseMars, 6);

//creation calque map
L.tileLayer('http://s3-eu-west-1.amazonaws.com/whereonmars.cartodb.net/mola-gray/{z}/{x}/{y}.png', {
    zoom: 3,
    tms: true,
}).addTo(map).setZIndex(0);
// ajout markeur
var markerBaseMars = L.marker(baseMars, { icon: city }).addTo(map);

//popup markeur
markerBaseMars.bindPopup('<h3>Capital martienne</h3>');




for (i = 0; zoneData.length > i; i++) {
    var discovey = zoneData[i]['created_at'].split('T');
    var times = discovey[1].split('.');

    switch (zoneData[i]['type_min']) {
        case 'Klingon':
            var marker = L.marker([zoneData[i]['geo_lat'], zoneData[i]['geo_lng']], { icon: klingon }).addTo(map);
            marker.bindPopup(`<h3>Minerai de type: ${zoneData[i]['type_min']}</h3>
            <h4>dangerosité: ${zoneData[i]['dangerousness']}</h4>
            <h5>découvert par ${zoneData[i]['name']} </h5>
            <p>le ${discovey[0]} à ${times[0]} H</p>
            <p>longitude: ${zoneData[i]['geo_lng']}</p>
            <p>latitude: ${zoneData[i]['geo_lat']}</p>
            <p>commentaire: ${zoneData[i]['commentary']}</p>`);

            break;

        case 'Chomdû':
            var marker = L.marker([zoneData[i]['geo_lat'], zoneData[i]['geo_lng']], { icon: anpe }).addTo(map);
            marker.bindPopup(`<h3>Minerai de type: ${zoneData[i]['type_min']}</h3>
            <h4>dangerosité: ${zoneData[i]['dangerousness']}</h4>
            <h5>découvert par ${zoneData[i]['name']} </h5>
            <p>le ${discovey[0]} à ${times[0]} H</p>
            <p>longitude: ${zoneData[i]['geo_lng']}</p>
            <p>latitude: ${zoneData[i]['geo_lat']}</p>
            <p>commentaire: ${zoneData[i]['commentary']}</p>`);
            break;

        case 'Perl':
            var marker = L.marker([zoneData[i]['geo_lat'], zoneData[i]['geo_lng']], { icon: perl }).addTo(map);
            marker.bindPopup(`<h3>Minerai de type: ${zoneData[i]['type_min']}</h3>
            <h4>dangerosité: ${zoneData[i]['dangerousness']}</h4>
            <h5>découvert par ${zoneData[i]['name']} </h5>
            <p>le ${discovey[0]} à ${times[0]} H</p>
            <p>longitude: ${zoneData[i]['geo_lng']}</p>
            <p>latitude: ${zoneData[i]['geo_lat']}</p>
            <p>commentaire: ${zoneData[i]['commentary']}</p>`);
            break;

        default:
            var marker = L.marker([zoneData[i]['geo_lat'], zoneData[i]['geo_lng']], { icon: warning }).addTo(map);
            marker.bindPopup(`<h3>Minerai de type: ${zoneData[i]['type_min']}</h3>
            <h4>dangerosité: ${zoneData[i]['dangerousness']}</h4>
            <h5>découvert par ${zoneData[i]['name']} </h5>
            <p>le ${discovey[0]} à ${times[0]} H</p>
            <p>longitude: ${zoneData[i]['geo_lng']}</p>
            <p>latitude: ${zoneData[i]['geo_lat']}</p>
            <p>commentaire: ${zoneData[i]['commentary']}</p>`);
            break;
    }
}