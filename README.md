# Readme

***
## QUICK-START
1. Avoir installé Composer en global.
2. Cloner le projet.
3. `composer install`
4. Créer un `.env` à la racine du projet et y intégrer le contenu du `env_exemple`.
5. Vous devez créer votre clef application avec`php artisan key:generate`.
6. Créer votre base de données. Pour ce faire, créer un fichier `database.sqlite` dans ce dossier database.
7. `php artisan migrate` => Créer les tables.
8. Pour lancer le serveur faites un `php artisan serve`.
9. Vous trouverez un dossier nommé BDD&Schéma_utilisateur&UML&maquette qui contient (surprise !!) le schéma de la BDD, le schéma utilisateur, l'uml d'activité et les maquettes. 